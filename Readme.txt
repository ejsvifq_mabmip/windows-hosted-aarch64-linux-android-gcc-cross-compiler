This is a canadian aarch64 android cross compiler for windows. Built by the fast_io library's author

Unix Timestamp:1642895842.163272441
UTC:2022-01-22T23:57:22.163272441Z

fast_io:
https://github.com/tearosccebe/fast_io.git

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	aarch64-linux-android


NOTICE for clang:
compiler-rt provides compiler runtime for clang 14. If you want to use this as a sysroot for clang, you need to copy compiler-rt to clang's installation directory manually

Also need -lgcc_eh -rtlib=libgcc -stdlib=libstdc++ for clang

for libc++, you need to -I${sysroot}/include/c++/v1

For using libunwind, you need to put -lunwind or -lgcc_eh for clang.
